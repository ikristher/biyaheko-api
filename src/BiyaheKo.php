<?php

namespace BiyaheKo;

use BiyaheKo\Exceptions\NoFlightsAvailable;
use BiyaheKo\Exceptions\ResponseException;
use BiyaheKo\Request\Domestic\AvailableFlightsRequest;
use BiyaheKo\Request\Domestic\BookedHistoryRequest;
use BiyaheKo\Request\Domestic\BookRequest;
use BiyaheKo\Request\Domestic\ReprintRequest;
use BiyaheKo\Request\Request;
use BiyaheKo\Request\Domestic\TaxDetailsRequest;
use BiyaheKo\Request\Domestic\TransactionStatusRequest;
use BiyaheKo\Response\Ticket;
use GuzzleHttp\Client;

class BiyaheKo
{

    protected $password;
    protected $request;
    /**
     * @var Client
     */
    protected $client;
    protected $username;
    const SUCCESS_RESPONSE = 1;
    const FAILED_RESPONSE = 0;

    const AVAILABLE_FLIGHTS_ENDPOINT = 'GetAvailability';
    const TAX_DETAILS_ENDPOINT = 'GetTax';

    const BOOK_ENDPOINT = 'GetBook';
    const TRANSACTION_STATUS = 'GetTransactionStatus';
    const BOOK_HISTORY = 'GetBookedHistory';
    const FARE_RULE = 'GetFareRule';
    const REPRINT = 'GetReprint';

    /**
     * Flights constructor.
     *
     * @param $url
     * @param $username
     * @param $password
     */
    public function __construct($url, $username, $password)
    {
        $this->username = $username;
        $this->password = $password;

        $this->client = new Client(['base_uri' => $url]);
    }

    public function getAuthentication()
    {
        return [
            'Authentication' => [
                'LoginId' => $this->username,
                'Password' => $this->password
            ]
        ];
    }

    protected function getFullRequest(Request $request)
    {
        return
            $this->getAuthentication() +
            $request->build();
    }

    /**
     * @param $response
     *
     * @throws NoFlightsAvailable
     * @throws ResponseException
     */
    protected function prepareException($response)
    {
        if (strpos($response['FailureRemarks'], 'Flight Not Available')) {
            throw new NoFlightsAvailable('No Flights Available', 0, $response);
        }
        throw new ResponseException($response['FailureRemarks'], 0, $response);
    }

    /**
     * @param $endpoint
     * @param Request $request
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws NoFlightsAvailable
     * @throws ResponseException
     */
    protected function call($endpoint, Request $request)
    {

        $response = $this->client->post($endpoint, ['json' => $this->getFullRequest($request)]);

        $response = json_decode($response->getBody(), true);

        if ($response['ResponseStatus'] != 1) {
            $this->prepareException($response);
        }

        return $response;
    }

    /**
     * @param AvailableFlightsRequest $request
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws NoFlightsAvailable
     * @throws ResponseException
     */
    public function searchFlights(AvailableFlightsRequest $request)
    {
        return $this->call(self::AVAILABLE_FLIGHTS_ENDPOINT, $request);
    }

    /**
     * @param TaxDetailsRequest $request
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws NoFlightsAvailable
     * @throws ResponseException
     */
    public function getTaxDetails(TaxDetailsRequest $request)
    {
        return $this->call(self::TAX_DETAILS_ENDPOINT, $request);
    }

    /**
     * @param BookRequest $request
     *
     * @return Ticket
     * @throws NoFlightsAvailable
     * @throws ResponseException
     */
    public function book(BookRequest $request)
    {
        $response = $this->call(self::BOOK_ENDPOINT, $request);

        return new Ticket($response, 'BookOutput');
    }

    public function getHistory(BookedHistoryRequest $request)
    {
        return $this->call(self::BOOK_HISTORY, $request);
    }

    public function getTransactionStatus(TransactionStatusRequest $request)
    {
        return $this->call(self::TRANSACTION_STATUS, $request);
    }

    public function getTicketDetails(ReprintRequest $request)
    {
        return $this->reprintTicket($request);
    }

    public function reprintTicket($request)
    {
        $response = $this->call(self::REPRINT, $request);

        return new Ticket($response, 'ReprintOutput');
    }

    public function getFareRule($request)
    {
        return $this->call(self::FARE_RULE, $request);
    }
}
