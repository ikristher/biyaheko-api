<?php


namespace BiyaheKo\Exceptions;


class ResponseException extends \Exception
{

    protected $response;

    public function __construct($message, $code = 0, $response = [])
    {
        parent::__construct($message, $code);
        $this->response = $response;
    }

    /**
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }
}