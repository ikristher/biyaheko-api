<?php


namespace BiyaheKo\Request;


abstract class Request
{
    protected $request;
    /**
     * @var
     */
    protected $userTrackId;

    /**
     * Request constructor.
     *
     * @param $userTrackId
     */
    public function __construct($userTrackId = '')
    {

        $this->userTrackId = $userTrackId;
        $this->request = $this->getDefaultFields();

    }

    abstract protected function getDefaultFields();
    abstract public function build();

    /**
     * @param mixed $userTrackId
     *
     * @return Request
     */
    public function setUserTrackId($userTrackId)
    {
        $this->userTrackId = $userTrackId;

        return $this;
    }

    public function toJson()
    {
        return json_encode($this->build());

    }
}