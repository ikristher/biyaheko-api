<?php


namespace BiyaheKo\Request;


use Carbon\Carbon;

trait BookRequestSetterTrait
{

    protected $passengers;
    protected $flightDetails;
    protected $tax;
    protected $userTrackingID;

    public function setCustomerDetails($details)
    {
        $this->request['CustomerDetails'] = array_merge($details, ['CountryId' => 63]);

        return $this;
    }

    public function setPassengersCount($adult = 1, $child = 0, $infant = 0)
    {

        $this->request['AdultCount']  = $adult;
        $this->request['ChildCount']  = $child;
        $this->request['InfantCount'] = $infant;

        return $this;
    }

    public function setBookingType($journey)
    {
        $this->request['BookingType'] = $journey;

        return $this;

    }

    public function setFlightDetails($segments, $tax, $trip = 1)
    {
        if ($trip == 1) {
            $this->flightDetails['Onward'] = $segments;
            $this->tax['Onward']           = $tax;

            return $this;
        }

        $this->flightDetails['Return'] = $segments;
        $this->tax['Return']           = $tax;

        return $this;
    }

    public function setPassengers($passengers)
    {

        $this->passengers = array_map(function($passenger){
            $passenger['Age'] = Carbon::now()->diffInYears(Carbon::parse($passenger['DateofBirth']));

            return $passenger;
        }, $passengers);

        return $this;
    }
}