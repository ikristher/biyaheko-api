<?php

namespace BiyaheKo\Request\Domestic;


use BiyaheKo\Request\Request;

class AvailableFlightsRequest extends Request
{

    protected $parameters;

    public function __construct(array $parameters)
    {
        parent::__construct();

        $this->parameters = $parameters;
    }
    protected function getDefaultFields()
    {
        return [
            "AvailabilityInput" => [
                "AdultCount"            => 1,
                "AirlineCode"           => "",
                "BookingType"           => "O",
                "ChildCount"            => 0,
                "ClassType"             => "ECONOMY",
                "InfantCount"           => 0,
                "JourneyDetails"        => [],
                "ResidentofPhilippines" => 1,
                "SeniorCitizen"         => "0",
                "Optional1"             => 0,
                "Optional2"             => 0,
                "Optional3"             => 0
            ]
        ];
    }

    public function setParameters($parameters)
    {

        $this->parameters = $parameters;

        return $this;
    }
    public function build()
    {
        $request = [
            'AvailabilityInput' => [
                'BookingType'    => $this->parameters['booking_type'],
                'JourneyDetails' => $this->buildJourneyDetails(),
                'ClassType'      => $this->parameters['class_type'],
                'AirlineCode'    => $this->parameters['airline_code'],
                'AdultCount'     => $this->parameters['adult'],
                'ChildCount'     => $this->parameters['child'],
                'InfantCount'    => $this->parameters['infant'],
            ]
        ];

        return $this->request = array_replace_recursive($this->getDefaultFields(), $request);

    }


    protected function buildJourneyDetails()
    {

        $journey = [
            [
                'Origin'      => $this->parameters['origin'],
                'Destination' => $this->parameters['destination'],
                'TravelDate'  => $this->parameters['departure']
            ]
        ];

        if ($this->parameters['booking_type'] == 'R') {
            array_push($journey, [
                'Origin'      => $this->parameters['destination'],
                'Destination' => $this->parameters['origin'],
                'TravelDate'  => $this->parameters['return']
            ]);
        }

        return $journey;
    }
}