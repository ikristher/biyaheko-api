<?php


namespace BiyaheKo\Request\Domestic;


use BiyaheKo\Request\Request;

class ReprintRequest extends Request
{
    private $pnr;

    /**
     * ReprintRequest constructor.
     *
     * @param string $string
     */
    public function __construct($pnr)
    {
        parent::__construct();
        $this->pnr = $pnr;
    }

    protected function getDefaultFields()
    {
        return [];
    }

    public function build()
    {
        return [
            'ReprintInput'=>[
                'HermesPNR' => $this->pnr
            ]
        ];
    }
}