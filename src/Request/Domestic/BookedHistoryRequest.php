<?php


namespace BiyaheKo\Request\Domestic;


use BiyaheKo\Request\Request;

class BookedHistoryRequest extends Request
{

    private $dateFrom;
    private $dateTo;
    /**
     * @var string
     */
    private $travelType;

    public function __construct($dateFrom, $dateTo, $travelType = 'D')
    {
        parent::__construct();

        $this->dateFrom   = $dateFrom;
        $this->dateTo     = $dateTo;
        $this->travelType = $travelType;
    }

    protected function getDefaultFields()
    {
        return [];
    }

    public function build()
    {
        return [
            'BookedHistoryInput' => [
                'FromDate'   => $this->dateFrom,
                'ToDate'     => $this->dateTo,
                'TravelType' => $this->travelType
            ]
        ];
    }
}