<?php


namespace BiyaheKo\Request\Domestic;


use BiyaheKo\Request\BookRequestSetterTrait;
use BiyaheKo\Request\Request;

class BookRequest extends Request
{

    use BookRequestSetterTrait;

    protected $request = [];

    public function build()
    {
        $this->request['TotalAmount']          = 0;
        $this->request['FlightBookingDetails'] = $this->buildFlightBookingDetails();

        return [
            'UserTrackId' => $this->userTrackId,
            'BookInput'   => $this->request,
        ];

    }


    public function getTotalAmount()
    {
        $this->build();

        return $this->request['TotalAmount'];

    }

    public function getTotalSegments()
    {
        $this->build();

        return $this->request['BookingType'] == 'O' ? 1 : 2;

    }

    public function getTotalPassengers()
    {
        $this->build();

        return $this->request['AdultCount'] + $this->request['ChildCount'] + $this->request['InfantCount'];

    }

    protected function getDefaultFields()
    {
        return [
            'CustomerDetails'       => [],
            'SpecialRemarks'        => '',
            'NotifyByMail'          => 0,
            'NotifyBySMS'           => 0,
            'AdultCount'            => 1,
            'ChildCount'            => 0,
            'InfantCount'           => 0,
            'SeniorCitizen'         => 0,
            'TotalAmount'           => 0,
            'BookingType'           => 'O',
            'FrequentFlyerRequest'  => null,
            'SpecialServiceRequest' => null,
            'FSCMealsRequest'       => null,
            'FlightBookingDetails'  => [],

        ];
    }


    protected function buildFlightBookingDetails()
    {
        $flightBookingDetails = [];
        foreach ($this->flightDetails as $journeyType => $flightDetail) {
            $details = $this->setupBookingDetails($flightDetail, $journeyType);
            array_push($flightBookingDetails, $details);
        }

        return $flightBookingDetails;

    }

    protected function buildPassengerBookingSegments($segments)
    {
        $bookingSegment = [];
        foreach ($segments as $segment) {
            array_push($bookingSegment, [

                "FlightId"            => $segment['FlightId'],
                "ClassCode"           => $segment['AvailPaxFareDetails'][0]['ClassCode'],
                "SpecialServiceCode"  => "",
                "FrequentFlyerId"     => "",
                "FrequentFlyerNumber" => "",
                "MealCode"            => "",
                "SeatPreferId"        => "",
                "SupplierId"          => $segment['SupplierId']
            ]);
        }

        return $bookingSegment;
    }

    protected function buildPassengers($segments, $journeyType)
    {
        $passengers    = [];
        $baggageAmount = 0;
        foreach ($this->passengers as $passenger) {

            $bookingSegments = $this->buildPassengerBookingSegments($segments);
            $passenger       = $passenger + [
                    'Nationality'         => '',
                    'BookingSegments'     => $bookingSegments,
                    "LCCBaggageRequest"   => $passenger['BaggageRequest'][$journeyType],
                    "LCCMealsRequest"     => null
                ];

            $this->buildPassengerBaggage($passenger);

            if ($passenger['LCCBaggageRequest'] != null) {
                $baggageAmount += $passenger['LCCBaggageRequest'][0]['BaggageAmount'];
            }

            unset($passenger['BaggageRequest']);

            array_push($passengers, $passenger);
        }

        return [$baggageAmount, $passengers];
    }

    protected function getJourneyTotalAmount($segment)
    {

        $adult  = $this->getFareDetails($segment, 'AdultTax') * $this->request['AdultCount'];
        $child  = $this->getFareDetails($segment, 'ChildTax') * $this->request['ChildCount'];
        $infant = $this->getFareDetails($segment, 'InfantTax') * $this->request['InfantCount'];


        return $adult + $child + $infant;
    }

    protected function getFareDetails($segment, $paxType = 'AdultTax')
    {
        $fareDetails = $segment['TaxResFlightSegments'][0];

        if (is_null($fareDetails[$paxType])) {
            return 0;
        }

        return $fareDetails[$paxType]['FareBreakUpDetails']['GrossAmount'];
    }

    /**
     * We need to match the number of segments with the baggage
     * So we will add null if segment is more than one
     *
     * @param $passenger
     */
    protected function buildPassengerBaggage(&$passenger)
    {


        if (is_null($passenger['LCCBaggageRequest'])) {
            return;
        }

        if (empty($passenger['LCCBaggageRequest'])) {
            $passenger['LCCBaggageRequest'] = null;

            return;
        }

        $diff = count($passenger['BookingSegments']) - count($passenger['LCCBaggageRequest']);

        while ($diff--) {
            array_push($passenger['LCCBaggageRequest'], null);
        }
    }

    /**
     * @param $flightDetail
     * @param $journeyType
     *
     * @return mixed
     */
    protected function setupBookingDetails($flightDetail, $journeyType)
    {
        $details = [
            "AirlineCode"      => $flightDetail[0]['AirlineCode'],
            "PaymentDetails"   => [
                "CurrencyCode" => "PHP",
                "Amount"       => 0
            ],
            "TourCode"         => "",
            "PassengerDetails" => []
        ];


        list($baggageAmount, $details['PassengerDetails']) = $this->buildPassengers($flightDetail, $journeyType);

        $details['PaymentDetails']['Amount'] = $totalAmount = $this->getJourneyTotalAmount($this->tax[$journeyType]) + $baggageAmount;

        $this->request['TotalAmount'] += $totalAmount;

        return $details;
    }
}