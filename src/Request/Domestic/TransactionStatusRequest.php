<?php


namespace BiyaheKo\Request\Domestic;


use BiyaheKo\Request\Request;

class TransactionStatusRequest extends Request
{

    protected function getDefaultFields()
    {
       return [];
    }

    public function build()
    {
        return [
            'UserTrackId' => $this->userTrackId
        ];
    }
}