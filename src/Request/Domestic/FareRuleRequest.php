<?php


namespace BiyaheKo\Request\Domestic;


use BiyaheKo\Request\Request;

class FareRuleRequest extends Request
{
    private $airlineCode;
    private $flightId;
    private $classCode;
    private $supplierId;

    /**
     * FareRuleRequest constructor.
     *
     * @param $userTrackId
     * @param $airlineCode
     * @param $flightId
     * @param $classCode
     * @param $supplierId
     */
    public function __construct($userTrackId, $airlineCode, $flightId, $classCode, $supplierId)
    {
        parent::__construct($userTrackId);
        $this->airlineCode = $airlineCode;
        $this->flightId    = $flightId;
        $this->classCode   = $classCode;
        $this->supplierId  = $supplierId;
    }

    protected function getDefaultFields()
    {
        return [];
    }

    public function build()
    {
        $this->request = [
            'AirlineCode' => $this->airlineCode,
            'FlightId'    => $this->flightId,
            'ClassCode'   => $this->classCode,
            'SupplierId'  => $this->supplierId
        ];

        return [
            'UserTrackId'   => $this->userTrackId,
            'FareRuleInput' => $this->request
        ];
    }
}