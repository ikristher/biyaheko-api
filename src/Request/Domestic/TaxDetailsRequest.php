<?php


namespace BiyaheKo\Request\Domestic;


use BiyaheKo\Request\Request;

class TaxDetailsRequest extends Request
{

    protected $parameters;

    public function __construct($userTrackId, array $parameters)
    {
        parent::__construct($userTrackId);

        $this->parameters = $parameters;
    }

    protected function getDefaultFields()
    {
        return  [
            'UserTrackId' => $this->userTrackId,
            'TaxInput'    => [
                "SeniorCitizen"        => "0",
                "TaxReqFlightSegments" => []
            ]
        ];
    }

    public function build()
    {

        $this->request = $this->getDefaultFields();
        foreach ($this->parameters as $segment) {
            array_push($this->request['TaxInput']['TaxReqFlightSegments'], [
                "FlightId"    => $segment['FlightId'],
                "ClassCode"   => $segment['AvailPaxFareDetails'][0]['ClassCode'],
                "AirlineCode" => $segment['AirlineCode'],
                "BasicAmount" => $segment['AvailPaxFareDetails'][0]['Adult']['BasicAmount'],
                "SupplierId"  => $segment['SupplierId'],
                "ETicketFlag" => 1,
            ]);
        }
        return $this->request;

    }
}