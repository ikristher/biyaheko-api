<?php


namespace BiyaheKo\Request\International;


use BiyaheKo\Request\Request;

class TaxDetailsRequest extends \BiyaheKo\Request\Domestic\TaxDetailsRequest
{




    protected function getDefaultFields()
    {
       return  [
           'UserTrackId' => $this->userTrackId,
           'TaxInput'    => [
               "TaxReqFlightSegments" => []
           ]
       ];
    }

}