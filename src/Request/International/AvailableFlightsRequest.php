<?php


namespace BiyaheKo\Request\International;


class AvailableFlightsRequest extends \BiyaheKo\Request\Domestic\AvailableFlightsRequest
{
    protected function getDefaultFields()
    {
        return [
            "AvailabilityInput" => [
                "AdultCount"            => 1,
                "AirlineCode"           => "",
                "BookingType"           => "O",
                "ChildCount"            => 0,
                "ClassType"             => "ECONOMY",
                "InfantCount"           => 0,
                "JourneyDetails"        => [],
                "ResidentofPhilippines" => 1,
                "Optional1"             => 0,
                "Optional2"             => 0,
                "Optional3"             => 0
            ]
        ];
    }


}