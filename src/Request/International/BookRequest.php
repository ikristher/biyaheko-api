<?php


namespace BiyaheKo\Request\International;


use BiyaheKo\Request\BookRequestSetterTrait;
use BiyaheKo\Request\Domestic\BookRequest as DomesticBookRequest;
use BiyaheKo\Request\Request;

class BookRequest extends DomesticBookRequest
{

    protected $request = [];


    protected function getDefaultFields()
    {
        return [
            'CustomerDetails'       => [],
            'SpecialRemarks'        => '',
            'NotifyByMail'          => 0,
            'NotifyBySMS'           => 0,
            'AdultCount'            => 1,
            'ChildCount'            => 0,
            'InfantCount'           => 0,
            'TotalAmount'           => 0,
            'BookingType'           => 'O',
            'FrequentFlyerRequest'  => null,
            'SpecialServiceRequest' => null,
            'FSCMealsRequest'       => null,
            'BookingDetails'        => [],

        ];
    }

    public function build()
    {
        $this->request['TotalAmount']    = 0;
        $this->request['BookingDetails'] = $this->buildFlightBookingDetails();

        return [
            'UserTrackId' => $this->userTrackId,
            'BookInput'   => $this->request,
        ];

    }


    protected function buildFlightBookingDetails()
    {

        $flightBookingDetails = $this->setupBookingDetails($this->flightDetails['Onward'], 'Onward');

        return $flightBookingDetails;

    }

    protected function buildPassengers($segments, $journeyType)
    {
        $passengers    = [];
        $baggageAmount = 0;
        foreach ($this->passengers as $passenger) {

            $bookingSegments = $this->buildPassengerBookingSegments($segments);
            $passenger       = $passenger + [
                    'Nationality'         => '',
                    'BookingSegments'     => $bookingSegments
                ];

            unset($passenger['BaggageRequest']);

            array_push($passengers, $passenger);
        }

        return [$baggageAmount, $passengers];
    }

    protected function buildPassengerBookingSegments($segments)
    {
        $bookingSegment = [];
        foreach ($segments as $index => $segment) {
            array_push($bookingSegment, [

                "FlightId"            => $segment['FlightId'],
                "ClassCode"           => $segment['AvailPaxFareDetails'][0]['ClassCode'],
                "SpecialServiceCode"  => "",
                "FrequentFlyerId"     => "",
                "FrequentFlyerNumber" => "",
                "MealCode"            => "",
                "SeatPreferId"        => "",
                "SupplierId"          => $segment['SupplierId'],
                "FlightTripType"      => $index < round(count($segments)/2) ? 'O' : 'R',
                "LCCBaggageRequest"   => null,
                "LCCMealsRequest"     => null
            ]);
        }

        return $bookingSegment;
    }
}