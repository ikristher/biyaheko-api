<?php


namespace BiyaheKo\Response;


class Ticket
{
    protected $ticketDetails;

    protected $response;

    /**
     * Ticket constructor.
     *
     * @param array $response
     * @param $key
     */
    public function __construct(array $response, $key)
    {

        $this->response = $response;
        $this->setTicketDetails($key);
    }

    public function getAirlineDetails()
    {
        return $this->ticketDetails['AirlineDetails'][0];

    }

    public function getResponse($key = '')
    {
        if ( ! $key) {
            return $this->response;
        }

        if (array_key_exists($key, $this->response)) {
            return $this->response[$key];
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getTicketDetails()
    {
        return $this->ticketDetails;
    }

    public function getUserTrackId()
    {
        return $this->response['UserTrackId'];
    }

    public function getPassengers()
    {
        return $this->ticketDetails['PassengerDetails'];

    }

    public function setTicketDetails($key = 'BookOutput')
    {
        if (array_key_exists($key, $this->response)) {
            $this->ticketDetails = $this->response[$key]['FlightTicketDetails'][0];
        }

    }


    public function toJson()
    {
        return json_encode($this->response);

    }
}