<?php

$dotenv = new Dotenv\Dotenv('../');
$dotenv->load();

return [
    "biyaheko" => [
        "url"      => getenv('BIYAHEKO_URL'),
        "username" => getenv('BIYAHEKO_USERNAME'),
        "password" => getenv('BIYAHEKO_PASSWORD'),
    ],

    "biyaheko_international" => [
        "url"      => getenv('BIYAHEKO_INTERNATIONAL_URL'),
        "username" => getenv('BIYAHEKO_INTERNATIONAL_USERNAME'),
        "password" => getenv('BIYAHEKO_INTERNATIONAL_PASSWORD'),
    ]
];