<?php


use PHPUnit\Framework\TestCase;

class BookFlightsTest extends BaseTestCase
{


    /**
     * @test
     * @throws Exception
     */
    public function it_can_book_ticket_for_single_passenger()
    {
        $results = $this->setUpSearch('02/16/2019', '', 'MNL', 'CEB');

        $flightSegment = $results['AvailabilityOutput']['AvailableFlights']['OngoingFlights'][0]['AvailSegments'];

        $request  = new \BiyaheKo\Request\Domestic\TaxDetailsRequest($results['UserTrackId'], $flightSegment);
        $response = $this->domesticClient->getTaxDetails($request);

        $request = new \BiyaheKo\Request\Domestic\BookRequest($results['UserTrackId']);

        $request->setCustomerDetails($this->setUpCustomerDetails())
                ->setPassengers($this->setUpPassengers())
                ->setFlightDetails($flightSegment, $response['TaxOutput']);

        $ticket = $this->domesticClient->book($request);

        $this->assertEquals(1, $ticket->getResponse('ResponseStatus'));
        $this->assertArrayHasKey('BookOutput', $ticket->getResponse());
    }

    /**
     * @test
     * @throws Exception
     */
    public function it_can_book_ticket_for_multiple_passenger()
    {
        $results = $this->setUpSearch('02/16/2019', '', 'MNL', 'CEB', 'O', 1, 1);

        $flightSegment = $results['AvailabilityOutput']['AvailableFlights']['OngoingFlights'][0]['AvailSegments'];

        $request  = new \BiyaheKo\Request\Domestic\TaxDetailsRequest($results['UserTrackId'], $flightSegment);
        $response = $this->domesticClient->getTaxDetails($request);

        $request = new \BiyaheKo\Request\Domestic\BookRequest($results['UserTrackId']);

        $request->setCustomerDetails($this->setUpCustomerDetails())
                ->setPassengersCount(1, 1)
                ->setPassengers($this->setUpPassengers(1, 1))
                ->setFlightDetails($flightSegment, $response['TaxOutput']);


        $ticket = $this->domesticClient->book($request);

        $this->assertEquals(1, $ticket->getResponse('ResponseStatus'));
        $this->assertArrayHasKey('BookOutput', $ticket->getResponse());
    }

    /**
     * @test
     * @throws Exception
     */
    public function it_can_book_ticket_for_single_passenger_round_trip()
    {
        $results = $this->setUpSearch('02/16/2019', '02/20/2019', 'MNL', 'CEB', 'R');

        $flightSegment1 = $results['AvailabilityOutput']['AvailableFlights']['OngoingFlights'][0]['AvailSegments'];
        $flightSegment2 = $results['AvailabilityOutput']['AvailableFlights']['ReturnFlights'][0]['AvailSegments'];

        $request   = new \BiyaheKo\Request\Domestic\TaxDetailsRequest($results['UserTrackId'], $flightSegment1);
        $response1 = $this->domesticClient->getTaxDetails($request);

        $request   = new \BiyaheKo\Request\Domestic\TaxDetailsRequest($results['UserTrackId'], $flightSegment2);
        $response2 = $this->domesticClient->getTaxDetails($request);

        $request = new \BiyaheKo\Request\Domestic\BookRequest($results['UserTrackId']);

        $request->setBookingType('R')
                ->setCustomerDetails($this->setUpCustomerDetails())
                ->setPassengers($this->setUpPassengers())
                ->setFlightDetails($flightSegment1, $response1['TaxOutput'])
                ->setFlightDetails($flightSegment2, $response2['TaxOutput'], 2);


        $ticket = $this->domesticClient->book($request);

        $this->assertEquals(1, $ticket->getResponse('ResponseStatus'));
        $this->assertArrayHasKey('BookOutput', $ticket->getResponse());

    }

    /**
     * @test
     * @throws Exception
     */
    public function it_can_book_ticket_for_multiple_passenger_round_trip()
    {
        $results = $this->setUpSearch('02/16/2019', '02/20/2019', 'MNL', 'CEB', 'R', 1, 1);

        $flightSegment1 = $results['AvailabilityOutput']['AvailableFlights']['OngoingFlights'][0]['AvailSegments'];
        $flightSegment2 = $results['AvailabilityOutput']['AvailableFlights']['ReturnFlights'][0]['AvailSegments'];

        $request   = new \BiyaheKo\Request\Domestic\TaxDetailsRequest($results['UserTrackId'], $flightSegment1);
        $response1 = $this->domesticClient->getTaxDetails($request);
        $request   = new \BiyaheKo\Request\Domestic\TaxDetailsRequest($results['UserTrackId'], $flightSegment2);
        $response2 = $this->domesticClient->getTaxDetails($request);

        $request = new \BiyaheKo\Request\Domestic\BookRequest($results['UserTrackId']);
        $request->setBookingType('R')
                ->setPassengersCount(1, 1)
                ->setCustomerDetails($this->setUpCustomerDetails())
                ->setPassengers($this->setUpPassengers(1, 1))
                ->setFlightDetails($flightSegment1, $response1['TaxOutput'])
                ->setFlightDetails($flightSegment2, $response2['TaxOutput'], 2);

        $ticket = $this->domesticClient->book($request);

        $this->assertEquals(1, $ticket->getResponse('ResponseStatus'));
        $this->assertArrayHasKey('BookOutput', $ticket->getResponse());

    }

    /**
     * @test
     * @throws Exception
     */
    public function it_can_book_ticket_for_single_passenger_with_connecting_flights()
    {
        $results = $this->setUpSearch('02/16/2019', '', 'CEB', 'PPS');

        $flightSegment = $results['AvailabilityOutput']['AvailableFlights']['OngoingFlights'][0]['AvailSegments'];

        $request  = new \BiyaheKo\Request\Domestic\TaxDetailsRequest($results['UserTrackId'], $flightSegment);
        $response = $this->domesticClient->getTaxDetails($request);

        $request = new \BiyaheKo\Request\Domestic\BookRequest($results['UserTrackId']);

        $request->setCustomerDetails($this->setUpCustomerDetails())
                ->setPassengers($this->setUpPassengers())
                ->setFlightDetails($flightSegment, $response['TaxOutput']);

        $ticket = $this->domesticClient->book($request);

        $this->assertEquals(1, $ticket->getResponse('ResponseStatus'));
        $this->assertArrayHasKey('BookOutput', $ticket->getResponse());
    }

    /**
     * @test
     * @throws \Exception
     */
    public function a_passenger_can_add_baggage_while_booking_connecting_flights()
    {
        $results = $this->setUpSearch('02/16/2019', '', 'CEB', 'PPS');

        foreach ($results['AvailabilityOutput']['AvailableFlights']['OngoingFlights'] as $flights) {
            if (count($flights['AvailSegments']) > 1) {
                $flightSegment = $flights['AvailSegments'];
                break;
            }
        }

        $request  = new \BiyaheKo\Request\Domestic\TaxDetailsRequest($results['UserTrackId'], $flightSegment);
        $response = $this->domesticClient->getTaxDetails($request);

        $request = new \BiyaheKo\Request\Domestic\BookRequest($results['UserTrackId']);

        $request->setCustomerDetails($this->setUpCustomerDetails())
                ->setPassengers(
                    $this->setUpPassengers(1, 0, 0, $response['TaxOutput']['Baggages'])
                );

        $request->setFlightDetails($flightSegment, $response['TaxOutput']);

        $ticket = $this->domesticClient->book($request);

        $this->assertEquals(1, $ticket->getResponse('ResponseStatus'));
        $this->assertArrayHasKey('BookOutput', $ticket->getResponse());
    }

    /**
     * @test
     * @throws \Exception
     */
    public function multiple_passengers_can_add_baggage_while_booking_roundtrip()
    {
        $results = $this->setUpSearch('02/16/2019', '02/20/2019', 'MNL', 'CEB', 'R', 1, 1);

        $flightSegment1 = $results['AvailabilityOutput']['AvailableFlights']['OngoingFlights'][0]['AvailSegments'];
        $flightSegment2 = $results['AvailabilityOutput']['AvailableFlights']['ReturnFlights'][0]['AvailSegments'];
        $request        = new \BiyaheKo\Request\Domestic\TaxDetailsRequest($results['UserTrackId'], $flightSegment1);
        $response1      = $this->domesticClient->getTaxDetails($request);
        $request        = new \BiyaheKo\Request\Domestic\TaxDetailsRequest($results['UserTrackId'], $flightSegment2);
        $response2      = $this->domesticClient->getTaxDetails($request);

        $request        = new \BiyaheKo\Request\Domestic\BookRequest($results['UserTrackId']);
        $request->setBookingType('R')
                ->setPassengersCount(1, 1)
                ->setCustomerDetails($this->setUpCustomerDetails())
                ->setPassengers(
                    $this->setUpPassengers(1, 1, 0, $response1['TaxOutput']['Baggages'], $response2['TaxOutput']['Baggages'])
                )
                ->setFlightDetails($flightSegment1, $response1['TaxOutput'])
                ->setFlightDetails($flightSegment2, $response2['TaxOutput'], 2);

        $ticket = $this->domesticClient->book($request);

        $this->assertEquals(1, $ticket->getResponse('ResponseStatus'));
        $this->assertArrayHasKey('BookOutput', $ticket->getResponse());
    }


    /**
     * @test
     */
    public function a_book_request_can_give_total_amount_when_asked()
    {
        $results = $this->setUpSearch('02/16/2019', '', 'CEB', 'PPS');

        $flightSegment = $results['AvailabilityOutput']['AvailableFlights']['OngoingFlights'][0]['AvailSegments'];

        $request  = new \BiyaheKo\Request\Domestic\TaxDetailsRequest($results['UserTrackId'], $flightSegment);
        $response = $this->domesticClient->getTaxDetails($request);

        $request = new \BiyaheKo\Request\Domestic\BookRequest($results['UserTrackId']);

        $request->setCustomerDetails($this->setUpCustomerDetails())
                ->setPassengers($this->setUpPassengers())
                ->setFlightDetails($flightSegment, $response['TaxOutput']);

        $this->assertInternalType(\PHPUnit\Framework\Constraint\IsType::TYPE_INT, $request->getTotalAmount());
    }

    /**
     * @test
     */
    public function a_book_request_can_give_number_of_segments_when_asked()
    {
        $results = $this->setUpSearch('02/16/2019', '', 'CEB', 'PPS');

        $flightSegment = $results['AvailabilityOutput']['AvailableFlights']['OngoingFlights'][0]['AvailSegments'];

        $request  = new \BiyaheKo\Request\Domestic\TaxDetailsRequest($results['UserTrackId'], $flightSegment);
        $response = $this->domesticClient->getTaxDetails($request);

        $request = new \BiyaheKo\Request\Domestic\BookRequest($results['UserTrackId']);

        $request->setCustomerDetails($this->setUpCustomerDetails())
                ->setPassengers($this->setUpPassengers())
                ->setFlightDetails($flightSegment, $response['TaxOutput']);

        $this->assertEquals(1, $request->getTotalSegments());

        $request->setBookingType('R');
        $this->assertEquals(2, $request->getTotalSegments());
    }
    /**
     * @test
     */
    public function a_book_request_can_give_number_of_passengers_when_asked()
    {
        $results = $this->setUpSearch('02/16/2019', '', 'CEB', 'PPS');

        $flightSegment = $results['AvailabilityOutput']['AvailableFlights']['OngoingFlights'][0]['AvailSegments'];

        $request  = new \BiyaheKo\Request\Domestic\TaxDetailsRequest($results['UserTrackId'], $flightSegment);
        $response = $this->domesticClient->getTaxDetails($request);

        $request = new \BiyaheKo\Request\Domestic\BookRequest($results['UserTrackId']);

        $request->setCustomerDetails($this->setUpCustomerDetails())
                ->setPassengers($this->setUpPassengers())
                ->setPassengersCount(1,1,1)
                ->setFlightDetails($flightSegment, $response['TaxOutput']);

        $this->assertEquals(3, $request->getTotalPassengers());
    }
}