<?php


use PHPUnit\Framework\TestCase;

class GetTaxDetailsTest extends BaseTestCase
{

    /**
     * @test
     * @throws Exception
     */
    public function it_can_get_flight_details_of_a_direct_flight()
    {

        $results       = $this->setUpSearch('02/22/2019');
        $flightSegment = $results['AvailabilityOutput']['AvailableFlights']['OngoingFlights'][0]['AvailSegments'];

        $request = new \BiyaheKo\Request\Domestic\TaxDetailsRequest($results['UserTrackId'], $flightSegment);

        $response = $this->domesticClient->getTaxDetails($request);

        $this->assertEquals(1, $response['ResponseStatus']);
        $this->assertArrayHasKey('TaxOutput', $response);
    }

    /**
     * @test
     */
    public function it_can_get_flight_details_of_a_connecting_flight()
    {
        $results = $this->setUpSearch('09/22/2018', '', 'CEB', 'PPS');

        foreach ($results['AvailabilityOutput']['AvailableFlights']['OngoingFlights'] as $flights) {

            if (count($flights['AvailSegments']) > 1) {
                $flightSegment = $flights['AvailSegments'];
                break;
            }
        }

        $request  = new \BiyaheKo\Request\Domestic\TaxDetailsRequest($results['UserTrackId'], $flightSegment);

        $response = $this->domesticClient->getTaxDetails($request);

        $this->assertEquals(1, $response['ResponseStatus']);
        $this->assertArrayHasKey('TaxOutput', $response);
    }


    /**
     * @test
     */
    public function it_can_get_fare_rule()
    {
        $results       = $this->setUpSearch('02/22/2019');
        $flightSegment = $results['AvailabilityOutput']['AvailableFlights']['OngoingFlights'][0]['AvailSegments'];

        $request = new \BiyaheKo\Request\Domestic\FareRuleRequest($results['UserTrackId'], $flightSegment[0]['AirlineCode'], $flightSegment[0]['FlightId'], $flightSegment[0]['AvailPaxFareDetails'][0]['ClassCode'],$flightSegment[0]['SupplierId']);

        $response = $this->domesticClient->getFareRule($request);

        $this->assertEquals(1, $response['ResponseStatus']);
        $this->assertArrayHasKey('FareRuleOutput', $response);

    }


    /**
     * @test
     * @throws Exception
     */
    public function it_can_get_flight_details_of_a_direct_international_flight()
    {

        $results       = $this->setUpInternationalSearch('02/22/2019');
        $flightSegment = $results['AvailabilityOutput']['AvailableFlight'][0]['AvailSegments'];

        $request = new \BiyaheKo\Request\International\TaxDetailsRequest($results['UserTrackId'], $flightSegment);

        $response = $this->internationalClient->getTaxDetails($request);

        $this->assertEquals(1, $response['ResponseStatus']);
        $this->assertArrayHasKey('TaxOutput', $response);
    }

    /**
 * @test
 */
    public function it_can_get_flight_details_of_a_connecting_international_flight()
    {
        $results = $this->setUpInternationalSearch('09/22/2018');

        foreach ($results['AvailabilityOutput']['AvailableFlight'] as $flights) {

            if (count($flights['AvailSegments']) > 1) {
                $flightSegment = $flights['AvailSegments'];
                break;
            }
        }

        $request  = new \BiyaheKo\Request\International\TaxDetailsRequest($results['UserTrackId'], $flightSegment);

        $response = $this->internationalClient->getTaxDetails($request);

        $this->assertEquals(1, $response['ResponseStatus']);
        $this->assertArrayHasKey('TaxOutput', $response);
    }

    /**
     * @test
     */
    public function it_can_get_flight_details_of_a_roundtrip_international_flight()
    {
        $results = $this->setUpInternationalSearch('09/22/2018','09/27/2018', 'MNL','KUL','R');

        foreach ($results['AvailabilityOutput']['AvailableFlight'] as $flights) {

            if (count($flights['AvailSegments']) > 2) {
                $flightSegment = $flights['AvailSegments'];
                break;
            }
        }
        $request  = new \BiyaheKo\Request\International\TaxDetailsRequest($results['UserTrackId'], $flightSegment);

        $response = $this->internationalClient->getTaxDetails($request);

        $this->assertEquals(1, $response['ResponseStatus']);
        $this->assertArrayHasKey('TaxOutput', $response);
    }

    /**
     * @test
     */
    public function it_can_get_flight_details_of_a_roundtrip_connecting_international_flight()
    {
        $results = $this->setUpInternationalSearch('09/22/2018','09/27/2018', 'MNL','KUL','R');

        $flightSegment = $results['AvailabilityOutput']['AvailableFlight'][0]['AvailSegments'];

        $request  = new \BiyaheKo\Request\International\TaxDetailsRequest($results['UserTrackId'], $flightSegment);

        $response = $this->internationalClient->getTaxDetails($request);

        $this->assertEquals(1, $response['ResponseStatus']);
        $this->assertArrayHasKey('TaxOutput', $response);
    }


    /**
     * @test
     */
    public function it_can_get_international_fare_rule()
    {
        $results       = $this->setUpInternationalSearch('02/22/2019');
        $flightSegment = $results['AvailabilityOutput']['AvailableFlight'][0]['AvailSegments'];

        $request = new \BiyaheKo\Request\Domestic\FareRuleRequest($results['UserTrackId'], $flightSegment[0]['AirlineCode'], $flightSegment[0]['FlightId'], $flightSegment[0]['AvailPaxFareDetails'][0]['ClassCode'],$flightSegment[0]['SupplierId']);

        $response = $this->internationalClient->getFareRule($request);

        $this->assertEquals(1, $response['ResponseStatus']);
        $this->assertArrayHasKey('FareRuleOutput', $response);

    }
}