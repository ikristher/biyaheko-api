<?php


use PHPUnit\Framework\TestCase;

class BaseTestCase extends TestCase
{


    use TestTrait;
    /**
     * @var \Faker\Generator
     */
    protected $faker;
    /**
     * @var BiyaheKo\BiyaheKo
     */
    protected $domesticClient;

    /**
     * @var BiyaheKo\BiyaheKo
     */
    protected $internationalClient;

    protected function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        parent::setUp();

        $this->faker          = \Faker\Factory::create();
        $this->domesticClient = new BiyaheKo\BiyaheKo(config('biyaheko.url'), config('biyaheko.username'), config('biyaheko.password'));


        $this->internationalClient = new BiyaheKo\BiyaheKo(config('biyaheko_international.url'), config('biyaheko_international.username'), config('biyaheko_international.password'));

    }
}