<?php


class ConfigurationTest extends \PHPUnit\Framework\TestCase {


	/**
	 * @test
	 */
	public function it_can_load_configuration_files() {

		$value = config('biyaheko.url');


		$this->assertEquals('http://115.248.39.80/BiyahekoAir/DomesticAir.svc/JSONService', $value);

	}

}
