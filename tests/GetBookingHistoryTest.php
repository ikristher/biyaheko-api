<?php


use BiyaheKo\Request\Domestic\BookedHistoryRequest;
use PHPUnit\Framework\TestCase;

class GetBookingHistoryTest extends BaseTestCase
{


    /**
     * @test
     */
    public function it_can_get_booking_history()
    {
        $request = new BookedHistoryRequest('07/01/2018', '07/30/2018','D');

        $result = $this->domesticClient->getHistory($request);

        $this->assertArrayHasKey('BookedHistoryOutput', $result);
        $this->assertEquals(1, $result['ResponseStatus']);

    }


    /**
     * @test
     */
    public function it_can_get_booking_history_for_international_flights()
    {
        $request = new BookedHistoryRequest('07/01/2018', '08/30/2018','I');

        $result = $this->internationalClient->getHistory($request);
        $this->assertArrayHasKey('BookedHistoryOutput', $result);
        $this->assertEquals(1, $result['ResponseStatus']);

    }
    /**
     * @test
     */
    public function it_can_get_transaction_status()
    {
        $request  = new \BiyaheKo\Request\Domestic\TransactionStatusRequest('YRKSNMO9769994996992791819490595371699554');

        $response = $this->domesticClient->getTransactionStatus($request);

        $this->assertArrayHasKey('TransactionStatusOutput', $response);
        $this->assertEquals(1, $response['ResponseStatus']);
    }

    /**
     * @test
     */
    public function it_can_reprint_itinerary()
    {
        $request = new \BiyaheKo\Request\Domestic\ReprintRequest('JSINE8');

        $ticket = $this->domesticClient->getTicketDetails($request);

        $this->assertEquals(1, $ticket->getResponse('ResponseStatus'));
        $this->assertArrayHasKey('ReprintOutput', $ticket->getResponse());
    }
}