<?php


class BookInternationalFlightTest extends BaseTestCase
{
    /**
     * @test
     * @throws Exception
     */
    public function it_can_book_ticket_for_single_passenger()
    {
        $results = $this->setUpInternationalSearch('02/16/2019', '', 'MNL', 'KUL');

        $flightSegment = $results['AvailabilityOutput']['AvailableFlight'][0]['AvailSegments'];

        $request  = new \BiyaheKo\Request\International\TaxDetailsRequest($results['UserTrackId'], $flightSegment);
        $response = $this->internationalClient->getTaxDetails($request);

        $request = new \BiyaheKo\Request\International\BookRequest($results['UserTrackId']);

        $request->setCustomerDetails($this->setUpCustomerDetails())
                ->setPassengers($this->setUpPassengers(1, 0, 0, null, null, true))
                ->setFlightDetails($flightSegment, $response['TaxOutput']);



        $ticket = $this->internationalClient->book($request);

        dd($ticket->toJson());
        $this->assertEquals(1, $ticket->getResponse('ResponseStatus'));
        $this->assertArrayHasKey('BookOutput', $ticket->getResponse());
    }
}