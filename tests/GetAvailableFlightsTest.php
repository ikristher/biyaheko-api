<?php


use BiyaheKo\Exceptions\NoFlightsAvailable;
use PHPUnit\Framework\TestCase;

class GetAvailableFlightsTest extends BaseTestCase
{


    /**
     * @test
     * @throws Exception
     */
    public function it_can_request_for_available_flights()
    {


        $parameters = [
            'origin'       => 'MNL',
            'destination'  => 'CEB',
            'departure'    => '02/20/2019',
            'return'       => '',
            'booking_type' => 'O',
            'adult'        => 0,
            'child'        => 2,
            'infant'       => 0,
            'class_type'   => 'ECONOMY',
            'airline_code' => '',
        ];
        $request    = new \BiyaheKo\Request\Domestic\AvailableFlightsRequest($parameters);

        $results    = $this->domesticClient->searchFlights($request);

        $this->assertArrayHasKey('UserTrackId', $results);
        $this->assertEquals(1, $results['ResponseStatus']);
        $this->assertArrayHasKey('OngoingFlights', $results['AvailabilityOutput']['AvailableFlights']);

    }

    /**
     * @test
     * @throws Exception
     */
    public function it_can_request_for_roundtrip_available_flights()
    {

        $parameters = [
            'origin'       => 'MNL',
            'destination'  => 'CEB',
            'departure'    => '01/20/2019',
            'return'       => '01/22/2019',
            'booking_type' => 'R',
            'adult'        => 1,
            'child'        => 0,
            'infant'       => 0,
            'class_type'   => 'ECONOMY',
            'airline_code' => '',
        ];

        $request = new \BiyaheKo\Request\Domestic\AvailableFlightsRequest($parameters);
        $results = $this->domesticClient->searchFlights($request);

        $this->assertArrayHasKey('UserTrackId', $results);
        $this->assertEquals(\BiyaheKo\BiyaheKo::SUCCESS_RESPONSE, $results['ResponseStatus']);
        $this->assertArrayHasKey('ReturnFlights', $results['AvailabilityOutput']['AvailableFlights']);

    }

    /**
     * @test
     * @throws Exception
     */
    public function it_throws_an_exception_if_there_is_no_flights_available()
    {

        $parameters = [
            'origin'       => 'MNL',
            'destination'  => 'BSO',
            'departure'    => '01/20/2019',
            'return'       => '01/22/2019',
            'booking_type' => 'R',
            'adult'        => 1,
            'child'        => 0,
            'infant'       => 0,
            'class_type'   => 'ECONOMY',
            'airline_code' => '5J',
        ];

        $this->expectException(\BiyaheKo\Exceptions\NoFlightsAvailable::class);
        $request = new \BiyaheKo\Request\Domestic\AvailableFlightsRequest($parameters);
        $this->domesticClient->searchFlights($request);

    }

    /**
     * @test
     * @throws Exception
     */
    public function an_exception_will_be_thrown_if_there_are_other_error_from_response()
    {

        $parameters = [
            'origin'       => 'MNL',
            'destination'  => 'CEB',
            'departure'    => '01/20/2019',
            'return'       => '01/22/2019',
            'booking_type' => 'R',
            'adult'        => 1,
            'child'        => 0,
            'infant'       => 0,
            'class_type'   => 'ECONOMY',
            'airline_code' => '5J',
        ];

        $this->expectException(\BiyaheKo\Exceptions\ResponseException::class);
        $request = new \BiyaheKo\Request\Domestic\AvailableFlightsRequest($parameters);
        $this->domesticClient->searchFlights($request);
    }

    /**
     * @test
     */
    public function it_can_get_available_international_flights()
    {

        $parameters = [
            'origin'       => 'MNL',
            'destination'  => 'KUL',
            'departure'    => '09/24/2018',
            'return'       => '',
            'booking_type' => 'O',
            'adult'        => 1,
            'child'        => 0,
            'infant'       => 0,
            'class_type'   => 'ECONOMY',
            'airline_code' => '',
        ];

        $request = new \BiyaheKo\Request\International\AvailableFlightsRequest($parameters);
        $results = $this->internationalClient->searchFlights($request);

        $this->assertArrayHasKey('UserTrackId', $results);
        $this->assertEquals(1, $results['ResponseStatus']);
        $this->assertArrayHasKey('AvailableFlight', $results['AvailabilityOutput']);
    }

    /**
     * @test
     * @throws Exception
     */
    public function it_can_request_for_roundtrip_available_international_flights()
    {

        $parameters = [
            'origin'       => 'MNL',
            'destination'  => 'KUL',
            'departure'    => '01/20/2019',
            'return'       => '01/22/2019',
            'booking_type' => 'R',
            'adult'        => 1,
            'child'        => 0,
            'infant'       => 0,
            'class_type'   => 'ECONOMY',
            'airline_code' => '',
        ];

        $request = new \BiyaheKo\Request\International\AvailableFlightsRequest($parameters);

        $results = $this->internationalClient->searchFlights($request);

        $this->assertArrayHasKey('UserTrackId', $results);
        $this->assertEquals(\BiyaheKo\BiyaheKo::SUCCESS_RESPONSE, $results['ResponseStatus']);
        $this->assertArrayHasKey('AvailableFlight', $results['AvailabilityOutput']);

    }
}