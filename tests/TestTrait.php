<?php


trait TestTrait
{
    protected function setUpCustomerDetails()
    {
        return [
            "Title"         => $this->faker->randomElement(['Mr.', 'Ms.']),
            "Name"          => "{$this->faker->firstName} {$this->faker->lastName}",
            "Address"       => '192 Bonuan Catacdang',
            "City"          => 'Dagupan',
            "CountryId"     => 63,
            "ContactNumber" => '9355604018',
            "EmailId"       => $this->faker->email,
            "PinCode"       => "002400"
        ];
    }

    protected function setUpPassengers($adult = 1, $child = 0, $infant = 0, $baggages1 = null, $baggages2 = null, $international = false)
    {
        $passengers = [];
        $totalCount = ['ADULT' => $adult, 'CHILD' => $child, 'INFANT' => $infant];
        foreach ($totalCount as $passengerType => $count) {
            for ($x = 0; $x < $count; $x++) {
                $passenger = [
                    "PassengerType"  => $passengerType,
                    "Title"          => $this->faker->randomElement(['Mr.', 'Ms.']),
                    "FirstName"      => $this->faker->firstName,
                    "LastName"       => $this->faker->lastName,
                    "Gender"         => $this->faker->randomElement(['M', 'F']),
                    "DateofBirth"    => $this->faker->date('m/d/Y'),
                    "Nationality"    => "Philippines",
                    "BaggageRequest" => ['Onward' => null, 'Return' => null]
                ];

                if ( ! $international) {
                    $passenger += [
                        "IdentityProofId"     => "",
                        "IdentityProofNumber" => "",
                        "IdProofPath"         => null,
                    ];
                } else {
                    $passenger += [
                        "PassportNumber"        => '0645470356',
                        "PassportExpiryDate"    => "12/09/2019",
                        "PassportIssuingCountry" => $this->faker->country,
                    ];
                }
                if ($baggages1 != null) {
                    $passenger['BaggageRequest']['Onward'] = [$this->faker->randomElement($baggages1)];
                }
                if ($baggages2 != null) {
                    $passenger['BaggageRequest']['Return'] = [$this->faker->randomElement($baggages2)];
                }
                array_push($passengers, $passenger);
            }
        }

        return $passengers;
    }

    /**
     * @param $departure
     * @param string $return
     * @param string $origin
     * @param string $destination
     * @param string $bookingType
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \BiyaheKo\Exceptions\NoFlightsAvailable
     * @throws \BiyaheKo\Exceptions\ResponseException
     */
    protected function setUpSearch($departure, $return = '', $origin = 'MNL', $destination = 'CEB', $bookingType = 'O', $adult = 1, $child = 0, $infant = 0)
    {


        $parameters = [
            'origin'       => $origin,
            'destination'  => $destination,
            'departure'    => $departure,
            'return'       => $return,
            'booking_type' => $bookingType,
            'adult'        => $adult,
            'child'        => $child,
            'infant'       => $infant,
            'class_type'   => 'ECONOMY',
            'airline_code' => 'Z2',
        ];

        $request = new \BiyaheKo\Request\Domestic\AvailableFlightsRequest($parameters);

        return $this->domesticClient->searchFlights($request);
    }

    /**
     * @param $departure
     * @param string $return
     * @param string $origin
     * @param string $destination
     * @param string $bookingType
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \BiyaheKo\Exceptions\NoFlightsAvailable
     * @throws \BiyaheKo\Exceptions\ResponseException
     */
    protected function setUpInternationalSearch($departure, $return = '', $origin = 'MNL', $destination = 'KUL', $bookingType = 'O', $adult = 1, $child = 0, $infant = 0)
    {


        $parameters = [
            'origin'       => $origin,
            'destination'  => $destination,
            'departure'    => $departure,
            'return'       => $return,
            'booking_type' => $bookingType,
            'adult'        => $adult,
            'child'        => $child,
            'infant'       => $infant,
            'class_type'   => 'ECONOMY',
            'airline_code' => 'Z2',
        ];

        $request = new \BiyaheKo\Request\International\AvailableFlightsRequest($parameters);

        return $this->internationalClient->searchFlights($request);
    }
}