<?php


if(!function_exists('config')){

    function config($key)
    {

        $array = new \Adbar\Dot(include '../config/services.php');

        return $array->get($key);
    }
}
