#PHP API for BiyaheKo Domestic Booking

##Includes the following API

- Search Flights
- Get Flight Tax Details
- Book a Flight
- Get Previous Transaction Status
- Get Book History
- Get Fare Rule


Please See test suite for documentation.
